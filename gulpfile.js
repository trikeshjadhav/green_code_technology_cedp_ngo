// ////////////////////////////////////////////////
// Require
// // /////////////////////////////////////////////
var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    browserSync = require('browser-sync'),
    del = require('del'),
    runSequence = require('run-sequence'),
    reload = browserSync.reload;



// ////////////////////////////////////////////////
// SASS Tasks
// // /////////////////////////////////////////////
gulp.task('sass:compile', function(){
  gulp.src('./app/css/**/*.sass')
  .pipe(plugins.sourcemaps.init())
  .pipe(plugins.sass({ outputStyle: 'uncompressed' }))
  .on('error', plugins.sass.logError)
  .pipe(plugins.autoprefixer({
    browsers: ['last 10 versions', 'ie >= 8', '> 1%'],
    cascade: false
  }))
  .pipe(gulp.dest('./app/css/'))
  .pipe(reload({stream:true}));

  gulp.src('./app/emails-dev/css/**/*.sass')
  gulp.src('./app/emails-dev/css/**/*.sass')
  .pipe(plugins.sourcemaps.init())
  .pipe(plugins.sass({ outputStyle: 'uncompressed' }))
  .on('error', plugins.sass.logError)
  .pipe(plugins.autoprefixer({
    browsers: ['last 10 versions', 'ie >= 8', '> 1%'],
    cascade: false
  }))
  .pipe(gulp.dest('./app/emails-dev/css/'))
  .pipe(reload({stream:true}));
});


// ////////////////////////////////////////////////
// Browser Sync Tasks
// // /////////////////////////////////////////////
gulp.task('serve', function(){
  browserSync.init({
    proxy: "greencodecedpngo.local",
	  port: "2000"
  });
  gulp.watch("./app/**/*.sass", ['sass:compile']);
  gulp.watch("./app/**/*.+(php|html)").on('change', browserSync.reload);
  gulp.watch(["./app/**/*.js","!./app/**/*.raw.js","!./app/**/*.plugin.js"]).on('change', browserSync.reload);
});


// ////////////////////////////////////////////////
// Email Tasks
// // /////////////////////////////////////////////
gulp.task('email:inline-css', function(){
  gulp.src('./app/emails-dev/*.html')
  .pipe(plugins.inlineCss({
    applyStyleTags: true,
  	applyLinkTags: true,
  	removeStyleTags: true,
  	removeLinkTags: true,
    removeHtmlSelectors: true
  }))
  .pipe(gulp.dest('./app/emails/'));
});



// ////////////////////////////////////////////////
// Error Handler
// // /////////////////////////////////////////////
function errorlog(err){
	console.error(err.message);
	this.emit('end');
}

// ////////////////////////////////////////////////
// Images Tasks
// // /////////////////////////////////////////////
gulp.task('images:optimize', function(){
  return gulp.src('./app/images/**/*.+(jpg|png|gif)')
  .pipe(plugins.imagemin())
  .pipe(gulp.dest('./app/images'));
});
// ////////////////////////////////////////////////
// BUILD Tasks
// // /////////////////////////////////////////////
gulp.task('build:removefolder', function(){
  return del([
    'build/**'
  ]);
});
gulp.task('build:copy', function(){
  gulp.src([
    './app/.htaccess'
  ])
  .pipe(gulp.dest('./build/'));
  return gulp.src([
    './app/**/*',
    '!app/**/*.psd',
    '!app/**/*.css',
    '!app/**/*.sass',
    '!app/**/*.raw.js'
  ])
  .pipe(gulp.dest('./build/'));
});

gulp.task('build:removeUnwantedFolderFromBuild', function(){
  del.sync(['build/css/**']);
  del.sync(['build/sass/**']);
  return del.sync(['build/emails-dev/**']);
});

gulp.task('js:build', function(){
  return gulp.src('./build/js/**/*.js')
  .pipe(plugins.uglify())
  .pipe(gulp.dest('./build/js'));
});
gulp.task('css:build', function() {
  return gulp.src('./build/css/**/*.css')
    .pipe(plugins.autoprefixer({
      browser: ['last 10 versions', 'ie >= 8', '> 1%'],
      cascade: false
    }))
    .pipe(plugins.cleanCss({debug: true, keepSpecialComments: 0}, function(details) {
        console.log(details.name + ': ' + details.stats.originalSize);
        console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(gulp.dest('./build/css'));
});


// gulp.task('startbuilding', ['css:build']);

// ////////////////////////////////////////////////
// Default Tasks
// // /////////////////////////////////////////////
gulp.task('default', ['serve']);
gulp.task('build:email', function() {
  return runSequence(
    'sass:compile',
    'email:inline-css'
  );
});
gulp.task('build', function() {
  return runSequence(
    'sass:compile',
    'email:inline-css',
    'build:removefolder',
    'build:copy',
    'build:removeUnwantedFolderFromBuild'
    // ['js:build','css:build']
  );
});



// ////////////////////////////////////////////////
// SOME SAMPLES FOR FUTURE REFERENCCE
// // /////////////////////////////////////////////







// ////////////////////////////////////////////////
// JADE Tasks
// // /////////////////////////////////////////////
// gulp.task('jade:compile', function(){
//   gulp.src('./app/**/*.jade')
//   .pipe(plugins.jade({
//     pretty: true
//   }))
//   .on('error', errorlog)
//   .pipe(gulp.dest('./app/'));
// });


// ////////////////////////////////////////////////
// Browser Sync Tasks
// // /////////////////////////////////////////////
// gulp.task('serve:app', function(){
//   browserSync.init({
//     proxy: "localhost"
//   });
//   gulp.watch("./app/**/*.sass", ['sass:compile']);
//   gulp.watch("./app/**/*.+(php|html)").on('change', browserSync.reload);
//   gulp.watch("./app/**/*.js").on('change', browserSync.reload);
// });


// ////////////////////////////////////////////////
// SASS Tasks
// // /////////////////////////////////////////////
// gulp.task('sass:compile', function(){
//   gulp.src('app/css/**/*.sass')
//   .pipe(plugins.sourcemaps.init())
//   .pipe(plugins.sass({ outputStyle: 'uncompressed' }))
//   .on('error', plugins.sass.logError)
//   .pipe(plugins.autoprefixer({
//       browser: ['last 10 versions', 'ie >= 8', '> 1%'],
//       cascade: false
//     }))
//   .pipe(gulp.dest('app/css/'))
//   .pipe(reload({stream:true}));
// });



// ////////////////////////////////////////////////
// COPY Tasks
// // /////////////////////////////////////////////
// gulp.task('copy', function(){
//   gulp.src('app/**')
//   .pipe(plugins.uglify())
//   .on('error', errorlog)
//   .pipe(gulp.dest('build'));
// });
